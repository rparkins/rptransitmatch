angular.module('starter.controllers', [])

// Authentication controller
// Put your login, register functions here
.controller('AuthCtrl', function($scope, $ionicHistory) {
  // hide back butotn in next view
  $ionicHistory.nextViewOptions({
    disableBack: true
  });
})

// Home controller
.controller('HomeCtrl', function($scope, $state) {})

// Information controller
.controller('InformationCtrl', function($scope, $state) {})

// Ticket controller
.controller('TicketCtrl', function($scope, $state) {
    // set default value for input form
    $scope.depDate = new Date();
    $scope.returnDate = new Date();
    $scope.aldult = 2;
    $scope.children = 1;
    $scope.infants = 0;
  })

// Nearby controller
.controller('NearbyCtrl', function($scope, $state, Places) {
  // get list places form Places model
  $scope.places = Places.all();

  function initialize() {
    // set up begining position
    var myLatlng = new google.maps.LatLng(21.0227358,105.8194541);

    // set option for map
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    // init map
    var map = new google.maps.Map(document.getElementById("map"),
      mapOptions);

    // assign to stop
    $scope.map = map;
  }
  // load map when the ui is loaded
  $scope.init = function() {
    initialize();
  }
})

// Weather controller
.controller('WeatherCtrl', function($scope, $state, Weather) {
   // get list days from Weather model
  $scope.days = Weather.all();

  // today weather
  $scope.today = $scope.days[0];
})

// Gallery controller
.controller('GalleryCtrl', function($scope, $state, Photos) {
  // get list photos from Photos service
  $scope.photos = Photos.all();

})

// Friends
.controller('FriendsCtrl', function($scope, $state, Friends) {
  // get list photos from Photos service
  $scope.friends = Friends.all();

})

.controller('TaxiCtrl', function($scope, $state) {
    function initialize() {
      // set up begining position
      var myLatlng = new google.maps.LatLng(21.0227358,105.8194541);

      // set option for map
      var mapOptions = {
        center: myLatlng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      // init map
      var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);

      // assign to stop
      $scope.map = map;
    }
    // load map when the ui is loaded
    $scope.init = function() {
      initialize();
    }
})

.controller('ShoppingCtrl', function($scope, $state, Places) {
  // get list places form Places model
  $scope.places = Places.all();
})

// Currency controller
.controller('CurrencyCtrl', function($scope, $state) {
  // default value
  $scope.from = 1000;

})

// Chat controller, view list chats and chat detail
.controller('ChatCtrl', function($scope, Chats) {
  $scope.chats = Chats.all();

  // remove a conversation
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };

  // mute a conversation
  $scope.mute = function(chat) {
    // write your code here
  }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats, $ionicScrollDelegate, $ionicActionSheet, $timeout) {
  //$scope.chat = Chats.get($stateParams.chatId);
  $scope.chat = Chats.get(0);

  $scope.sendMessage = function() {
    var message = {
      type: 'sent',
      time: 'Just now',
      text: $scope.input.message
    };

    $scope.input.message = '';

    // push to massages list
    $scope.chat.messages.push(message);

    $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
  };

  // hover menu
  $scope.onMessageHold = function(e, itemIndex, message) {
    // show hover menu
    $ionicActionSheet.show({
      buttons: [
        {
          text: 'Copy Text'
        }, {
          text: 'Delete Message'
        }
      ],
      buttonClicked: function(index) {
        switch (index) {
          case 0: // Copy Text
            //cordova.plugins.clipboard.copy(message.text);

            break;
          case 1: // Delete
            // no server side secrets here :~)
            $scope.chat.messages.splice(itemIndex, 1);
            break;
        }

        return true;
      }
    });
  };

});
