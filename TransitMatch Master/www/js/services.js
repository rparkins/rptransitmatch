angular.module('starter.services', [])

.factory('Chats', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [
    {
      id: 0,
      name: 'Ben Sparrow',
      lastText: 'You on your way?',
      face: 'img/thumb/ben.png',
      messages: [
        {
          type: 'received',
          text: 'Hey, How are you? wanna hang out this friday?',
          image: '',
          time: 'Thursday 05:55 PM'
        },
        {
          type: 'sent',
          text: 'Good, Yes sure why not :D',
          image: '',
          time: 'Thursday 05:56 PM'
        },
        {
          type: 'received',
          text: 'Check out this view from my last trip',
          image: '/img/thumb/canada.jpg',
          time: 'Thursday 05:57 PM'
        },
        {
          type: 'sent',
          text: 'Looks Great is that view in Canada?',
          image: '',
          time: 'Thursday 05:58 PM'
        },
        {
          type: 'received',
          text: 'Yes, it\'s in Canada',
          image: '',
          time: 'Thursday 05:57 PM'
        }
      ]
    },
    {
      id: 1,
      name: 'Max Lynx',
      lastText: 'Hey, it\'s me',
      face: 'img/thumb/max.png'
    },
    {
      id: 2,
      name: 'Adam Bradleyson',
      lastText: 'I should buy a boat',
      face: 'img/thumb/adam.jpg'
    },
    {

      d: 3,
      name: 'Perry Governor',
      lastText: 'Look at my mukluks!',
      face: 'img/thumb/perry.png'
    },
    {
      id: 4,
      name: 'Mike Harrington',
      lastText: 'This is wicked good ice cream.',
      face: 'img/thumb/mike.png'
    },
    {
      id: 5,
      name: 'Ben Sparrow',
      lastText: 'You on your way?',
      face: 'img/thumb/ben.png'
    },
    {
      id: 6,
      name: 'Max Lynx',
      lastText: 'Hey, it\'s me',
      face: 'img/thumb/max.png'
    }
  ];

  return {
    all: function () {
      return chats;
    },
    remove: function (chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function (chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('Places', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var places = [
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r1_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
    {
      name: "Hanoi hotel",
      thumb: "img/thumb/r2_thumb.jpg",
      type: "Hotel",
      address: "No 2, Hoang Sa",
      distance: "120m",
      rate: 9.1
    },
    {
      name: "Chipa Chipa BBQ",
      thumb: "img/thumb/r3_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Truong Sa",
      distance: "220m",
      rate: 7.5
    },
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r4_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r5_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r6_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
    {
      name: "Sumo BBQ",
      thumb: "img/thumb/r7_thumb.jpg",
      type: "Restaurant",
      address: "No 2, Loseby",
      distance: "20m",
      rate: 8.1
    },
  ];

  return {
    all: function () {
      return places;
    },
    remove: function (post) {
      places.splice(places.indexOf(post), 1);
    },
    get: function (postId) {
      for (var i = 0; i < places.length; i++) {
        if (places[i].id === parseInt(postId)) {
          return places[i];
        }
      }
      return null;
    }
  };
})

.factory('Weather', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var days = [
    {
      weekDay: "Monday",
      minTemp: 10,
      maxTemp: 20,
      currentTemp: 19,
      feelLike: 19,
      status: "Cloudy",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
    {
      weekDay: "Tuesday",
      minTemp: 11,
      maxTemp: 20,
      currentTemp: 18,
      feelLike: 18,
      status: "Cloud",
      wind: {
        speed: 21,
        direction: "up-left"
      }
    },
    {
      weekDay: "Wednesday",
      minTemp: 15,
      maxTemp: 25,
      currentTemp: 22,
      feelLike: 22,
      status: "Fog",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
    {
      weekDay: "Thursday",
      minTemp: 10,
      maxTemp: 20,
      currentTemp: 19,
      feelLike: 19,
      status: "Day-sunny",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
    {
      weekDay: "Friday",
      minTemp: 10,
      maxTemp: 20,
      currentTemp: 19,
      feelLike: 19,
      status: "Day-lightning",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
    {
      weekDay: "Saturday",
      minTemp: 10,
      maxTemp: 20,
      currentTemp: 19,
      feelLike: 19,
      status: "Cloudy",
      wind: {
        speed: 21,
        direction: "down-right"
      }
    },
    {
      weekDay: "Sunday",
      minTemp: 5,
      maxTemp: 15,
      currentTemp: 15,
      feelLike: 12,
      status: "Rain",
      wind: {
        speed: 21,
        direction: "up-right"
      }
    },
  ];

  return {
    all: function () {
      return days;
    },
    remove: function (post) {
      days.splice(days.indexOf(post), 1);
    },
    get: function (postId) {
      for (var i = 0; i < days.length; i++) {
        if (days[i].id === parseInt(postId)) {
          return days[i];
        }
      }
      return null;
    }
  };
})

// Photos service
.factory('Photos', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var photos = [
    {
      src: "img/gallery/p1.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p2.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p3.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p4.png",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p5.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p6.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p7.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p8.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
    {
      src: "img/gallery/p9.jpg",
      sub: "Top 5 Shopping Spots in New York City"
    },
  ];

  return {
    all: function () {
      return photos;
    },
    remove: function (post) {
      photos.splice(photos.indexOf(post), 1);
    }
  };
})
// Friends service
.factory('Friends', function () {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [
    {
      id: 0,
      name: 'Ben Sparrow',
      face: 'img/thumb/ben.png'
    },
    {
      id: 1,
      name: 'Max Lynx',
      face: 'img/thumb/max.png'
    },
    {
      id: 2,
      name: 'Adam Bradleyson',
      face: 'img/thumb/adam.jpg'
    },
    {

      d: 3,
      name: 'Perry Governor',
      face: 'img/thumb/perry.png'
    },
    {
      id: 4,
      name: 'Mike Harrington',
      face: 'img/thumb/mike.png'
    },
    {
      id: 5,
      name: 'Ben Sparrow',
      face: 'img/thumb/ben.png'
    },
    {
      id: 6,
      name: 'Max Lynx',
      face: 'img/thumb/max.png'
    }
  ];

  return {
    all: function () {
      return friends;
    },
    remove: function (post) {
      friends.splice(friends.indexOf(post), 1);
    }
  };
})
