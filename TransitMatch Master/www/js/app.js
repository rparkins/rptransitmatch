// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
// 'nl2brl' is found in lib/angular-nl2br
// 'monospaced.elastic' is found in lib/angular-elastic
// 'ion-gallery' is found in lib/ion-gallery
//
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'nl2br', 'monospaced.elastic', 'ion-gallery'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

   // login screen
  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'AuthCtrl'
  })

  // register screen
  .state('register', {
    url: '/register',
    templateUrl: 'templates/register.html',
    controller: 'AuthCtrl'
  })

  // Home screen
  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl'
  })

  // City information
  .state('information', {
    url: '/information',
    templateUrl: 'templates/information.html',
    controller: 'InformationCtrl'
  })

  // Show nearby places
  .state('nearby', {
    url: '/nearby',
    templateUrl: 'templates/nearby.html',
    controller: 'NearbyCtrl'
  })

  // Book flight ticket
  .state('ticket', {
    url: '/ticket',
    templateUrl: 'templates/ticket.html',
    controller: 'TicketCtrl'
  })

   // Weather
  .state('weather', {
    url: '/weather',
    templateUrl: 'templates/weather.html',
    controller: 'WeatherCtrl'
  })

   // Gallery
  .state('gallery', {
    url: '/gallery',
    templateUrl: 'templates/gallery.html',
    controller: 'GalleryCtrl'
  })

  // Friends nearby
  .state('friends', {
    url: '/friends',
    templateUrl: 'templates/friends.html',
    controller: 'FriendsCtrl'
  })

  // Book a taxi
  .state('taxi', {
    url: '/taxi',
    templateUrl: 'templates/taxi.html',
    controller: 'TaxiCtrl'
  })

  // Find a shop
  .state('shopping', {
    url: '/shopping',
    templateUrl: 'templates/shopping.html',
    controller: 'ShoppingCtrl'
  })

  // Convert currency
  .state('currency', {
    url: '/currency',
    templateUrl: 'templates/currency.html',
    controller: 'CurrencyCtrl'
  })

  // Chat list
  .state('chats', {
    url: '/chats',
    templateUrl: 'templates/chats.html',
    controller: 'ChatCtrl'
  })

  .state('chat-detail', {
    url: '/chats/:chatId',
    templateUrl: 'templates/chat-detail.html',
    controller: 'ChatDetailCtrl'
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});
